import React from 'react';
import logo from './logo.svg';
import './App.css';
import Viewproduct from './components/Viewproduct';
import Products from './components/Products';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


class App extends React.Component {
  
  render() {
   return(
      <Router>
          
          <Switch>
              <Route exact path='/' component={Products} />
              <Route path='/viewproduct' component={Viewproduct} />
          </Switch>
      </Router>
   );
           
  }
}



export default App;
