import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import {Link} from 'react-router-dom';

class Viewproduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view: false,
      productss: [],
      result: 'Laptop',
      response: []
    };
  }

  

  
  componentDidMount() {
    fetch('http://localhost:5000/products')
    .then(res => res.json())
    .then((data1) => {
      this.setState({
        productss: data1,
      });
    })
      .catch(console.log)
  }

  

  render() {
     let productName = this.props.location.state.pname;
      console.log('true  -----   ' + productName);
    //   this.getProduct();
        let productdata = this.state.productss; console.log('KID---------'+productdata);
     //let productName = this.prop.name;
      const ProductView = productdata.filter((pvdata) => pvdata.Name==productName);
      console.log('LOLLLLLL----------'+ProductView);
      return(
        
      <div className="container" style={{"margin-top":"50px"}}>
        <h1 style={{"text-align":"center"}}><u>Product Details</u></h1>
      <div className="row">
        <div className="col-md-12">
        <Link to={`/`}><button className="btn btn-primary" style={{"float":"right"}}>View Product</button></Link>
        </div>
        </div>
      <hr/>
        {ProductView.map( pview => (
          <div className="card" style={{"padding":"30px"}}>
        <div className="row">
          <div className="col-md-6">
            <img className="img-fluid" width="200" src={pview.image}/>
          </div>
          <div className="col-md-6">
            <h1>Category: {pview.category}</h1>
            <h2>Product Name: {pview.Name}</h2>
            <h3>Product Description: {pview.description}</h3>
          </div>
        </div>
        </div>
        ))}
      </div>
    )
  
  
  }
}


export default Viewproduct;
