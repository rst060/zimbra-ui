import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import Viewproduct from './Viewproduct';
import { Redirect } from 'react-router';

class Products extends React.Component {
  constructor() {
    super();
    this.state = {
      view: false,
      category: [],
      result: 'Laptop',
      response: [],
      name:''
    };
    //this.onProductDetail = this.onProductDetail.bind(this);
  }

//   onProductDetail(event) {
//     let pname = event.target.value;
//     console.log(pname);
//     this.setState({
//       view: true,
//       name:pname
//     })
//     //return <Viewproduct name1={this.state.name}/>

//   }
handleClick = (event) => {
    event.stopPropagation();
    this.setState({
              view: true,
               name:event.target.text
             });
    console.log(event.target.text);
  }

  handleSelectChange = (event) => {
    this.setState({
      result: event.target.value
    })

  }

  componentDidMount() {
    let initialProduct = [];
    fetch('http://localhost:4000/categories')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          category: data,
        });
        console.log(initialProduct);

      })
      .catch(console.log)
  }

  componentWillMount() {
    //let myArg = this.state.result;
    this.getProduct();
  }

  getProduct() {
    fetch('http://localhost:5000/products')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          response: data,
        });
      })
      .catch(console.log)
  }

  // viewProduct(){
  //   if(this.state.view){
  //     this.getProduct();
  //     let productdata = this.state.response;
  //   let productName = this.state.result;
  //   const ProductView = productdata.filter((pvdata) => pvdata.name==productName);
  //     return(
  //     <div className="container">
  //       {ProductView.map( pview => (
  //       <div className="row">
  //         <div className="col-md-6">
  //           <img className="img-fluid" width="200" src={pview.image}/>
  //         </div>
  //         <div className="col-md-6">
  //           <h1>Category: {pview.category}</h1>
  //           <h2>Product Name: {pview.name}</h2>
  //           <h3>Product Description: {pview.description}</h3>
  //         </div>
  //       </div>
  //       ))}
  //     </div>
  //   )}
  // }

  render() {
    if (this.state.view) {
        //return <Redirect push  to={"/viewproduct/" + this.state.name } />;
       return <Redirect to={{
            pathname: '/viewproduct',
            state: { pname: this.state.name }
        }}
        />
      }
    let categorydata = this.state.category;
    let optionItems = categorydata.map((categorydata) =>
      <option key={categorydata.id}>{categorydata.name}</option>
    );
    const productdata = this.state.response;console.log('KIDssss----- ' +productdata);
    let filterVal = this.state.result;
    const filteredProduct = productdata.filter((pdata) => pdata.category==filterVal);
    console.log(filteredProduct);
    return (
      <div className="container">
          <h1 className="text-center">Product Listing Page</h1>
              <div className="form-group">
                <select className="form-control" onClick={this.handleSelectChange}>
                  {optionItems}
                </select>
              </div>
          <hr/>
            <div className="row">
              {filteredProduct.map( product => (
                <div className="col-md-3 col-sm-4 col-xs-6 text-center" style={{"font-weight":"bold","cursor":"pointer","color":"blue"}}>
                  <img className="img-responsive" width="250" src={product.image} />
                  <a className="card-title text-center pointer" onClick={this.handleClick}>{product.Name}</a>
                  </div>
                  ))}
            </div>
      </div>
    );
           
  }
}
// class Viewproduct1 extends React.Component {
//   render() {
//     console.log('KIDS');
//     return <h1>{this.props.name1}</h1>;
//   }
// }



export default Products;
